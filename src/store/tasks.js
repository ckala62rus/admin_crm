import axios from 'axios';

const state = {
    blocks: {},
};

const getters = {
    GET_TASKS(state) {
        return state.blocks;
    },
};

const mutations = {
    SET_TASKS: (state, payload) => {
        state.blocks = payload;
    },
};

const actions = {

    GET_ALL_TASKS: (commit, payload) => {
        commit.commit('SET_TASKS', payload);
    },

    CREATE_TASKS: (commit, payload) => {
        axios.get('tasks?project_id=' + payload.project_id).then((res) => {
            commit.commit('SET_TASKS', res.data.data)
        })
    },

};

export default {
    state,
    getters,
    mutations,
    actions,
};
