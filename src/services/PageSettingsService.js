import Axios from 'axios';

export default {

    getSettings(page) {
        return Axios.get(`/admin/page-settings/${page}`);
    },

    setSettings(page, data) {
        return Axios.post(`/admin/page-settings/${page}`, data);
    }

}