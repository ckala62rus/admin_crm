import Axios from 'axios';

export default {

    getCategories(params) {

        let limit = params.limit || 25;

        return Axios.get('/admin/categories?limit=' + limit);
    },

    createCategory(data) {        
        return Axios.post('/admin/categories', data);
    },

    updateCategory(id, data) {        
        return Axios.put(`/admin/categories/${id}`, data);
    },

    deleteCategory(id) {        
        return Axios.delete(`/admin/categories/${id}`);
    }
}