const state = {
    projects: {},
};

const getters = {
    GET_PROJECTS(state) {
        return state.projects;
    },
};

const mutations = {
    SET_PROJECTS: (state, payload) => {
        state.projects = payload;
    },
};

const actions = {

    GET_ALL_PROJECTS: (commit, payload) => {
        commit.commit('SET_TASKS', payload);
    },

};

export default {
    state,
    getters,
    mutations,
    actions,
};
