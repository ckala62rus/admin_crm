
import ServicesService from '@/services/ServicesService';

const state = {
    services: []
};

const getters = {
    getServices: (context) => {
        return context.services
    },

    getServiceOptions: (context) => {
        return context.services.map(el => {
            return {
                id: el,
                title: el.toUpperCase()
            }
        })
    }
}

const mutations = {

    SET_SERVICES: (context, services) => {
        context.services = services
    },

};

const actions = {

    getServices: (context) => {

        if (context.getters.getServices.length) {
            return context.commit('SET_SERVICES', context.getters.getServices)
        }

        return ServicesService.getServices().then((response) => {            
            context.commit('SET_SERVICES', response.data.data)
            
        })
    }

};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};