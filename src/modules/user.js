import Vue from 'vue';
import Axios from 'axios';

const state = {
    user: {},
    avatar: false
};

const getters = {

    USER_NAME: state => {
        return state.user ? state.user.name : null;
    },
    USER_EMAIL: state => {
        return state.user.email;
    },
    USER_ID: state => {
        return state.user ? state.user.id : null;
    },
    USER_POSITION: state => {
        return state.user.user.position.name;
    },
    GET_PROFILE: state => {
        return state.user.profile;
    },
    USER_ROLE: state => {
        return state.user.role;
    },
    GET_AVATAR: state => {
        if(state.avatar === false) {
            Axios.get('/users/get-avatar').then((response) => {
                state.avatar = response.data.avatar.avatar;
            })
        }
        return state.avatar;
    }
};

const mutations = {

    SET_USER: (state, payload) => {
        state.user = payload;
    },

    UPDATE_USER_DATA: (state) => {
        state.user = Vue.$jwt.decode(localStorage.getItem('access_token'))
    },

    SET_AVATAR: (state, payload) => {
        state.avatar = payload;
    }

};

const actions = {

    GET_ME: async (context) => {
        await Axios.post('/auth/me').then((response) => {
            context.commit('SET_USER', response.data.user);
        });
    },

    UPDATE_USER_AVATAR: async (context, payload) => {
        Axios.post('users/update-avatar', payload).then((response) => {
            context.commit('SET_AVATAR', response.data.avatar);
        })
    },

};

export default {
    state,
    getters,
    mutations,
    actions,
};