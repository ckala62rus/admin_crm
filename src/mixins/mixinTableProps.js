export const mixinTableProps = {
    data() {
        return {
            options: {
                texts: {
                    count: "Показано {from}-{to} из {count} записей|{count} записи|Одна запись",
                    first: 'First',
                    last: 'Last',
                    filter: "Поиск:",
                    filterPlaceholder: "Введите запрос",
                    limit: "Записей:",
                    page: "Страница:",
                    noResults: "Нет данных для отображения",
                    filterBy: "Фильтр по {column}",
                    loading: 'Загружка. Пожалуйста ждите...',
                    defaultOption: 'Выбрать {column}',
                    columns: 'Колонки'
                },
                filterable: true,
                skin: 'table table-bordered table-checkable dataTable no-footer dtr-inline collapsed',
                sortIcon: {
                    base: 'la',
                    up: 'la-long-arrow-up text-success pt-1',
                    down: 'la-long-arrow-down text-success pt-1',
                    is: 'la-arrows-v text-muted pt-1'
                }
            }
        }
    }
}