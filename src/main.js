require('./bootstrap');

import Vue from 'vue'
import App from './App.vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

import VModal from 'vue-js-modal'
Vue.use(VModal, {
    dynamic:true,
    dynamicDefaults: {
        height: 'auto',
        adaptive: true
      }
})

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/ru-RU'
locale.el.pagination.pagesize = '';
Vue.use(ElementUI, {locale});

import {ServerTable, ClientTable} from 'vue-tables-2';
Vue.use(ServerTable, {}, false, 'bootstrap4');
Vue.use(ClientTable, {}, false, 'bootstrap4');

import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'

Vue.use(PerfectScrollbar)

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {
    hideModules: { "table": true },
}); // config is optional. more below
import "vue-wysiwyg/dist/vueWysiwyg.css";

import VueRouter from 'vue-router'
Vue.use(VueRouter);

import Vuex from 'vuex';
Vue.use(Vuex);

import {store} from './store'

const VueInputMask = require('vue-inputmask').default 
Vue.use(VueInputMask)

import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
Vue.use(DatePicker)

import vueKanban from 'vue-kanban'
Vue.use(vueKanban)


Vue.use(require('vue-moment'));

Vue.config.productionTip = false

const requireComponent = require.context(
  './components/base',
  true,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(
      fileName
        .split('/')
        .pop()
        .replace(/\.\w+$/, '')
    )
  )

  Vue.component(
    componentName,  
    componentConfig.default || componentConfig
  )
})

const Index = () => import('./views/Index');
const AuthPage = () => import('./views/AuthPage');

const ProfileIndex = () => import('./views/profile/Index');
const ProfileSettings = () => import('./views/profile/Settings');
const AccountSettings = () => import('./views/profile/AccountSettings');

const Users = () => import('./views/users/Users');

const UsersForm = () => import('./views/users/UserForm');

const RoleIndex = () => import('./views/role/Index');

const RoleForm = () => import('./views/role/RoleForm');

const RoleCreate = () => import('./views/role/RoleCreate');

const IndexKanban = () => import('./views/kanban/Index');

const IndexProjects = () => import('./views/project/Index');

const ProjectsTable = () => import('./views/project/ProjectsTable');

const IndexTasks = () => import('./views/tasks/Index');

const IndexStatistic = () => import('./views/statistic/index');

const IndexCostCategories = () => import('./views/cost-categories/Index');

const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/auth', name: 'auth', component: AuthPage},
        {path: '/', name: 'index', component: Index},

        {path: '/me/overview/', name: 'profile', component: ProfileIndex},
        {path: '/me/settings/', name: 'profile-settings', component: ProfileSettings},
        {path: '/me/account/settings/', name: 'account-settings', component: AccountSettings},
        
        {path: '/users', name: 'users', component: Users},
        {path: '/users/:id', name: 'user-edit', component: UsersForm},

        {path: '/roles', name: 'roles', component: RoleIndex},
        {path: '/roles/create', name: 'role-create', component: RoleCreate},
        {path: '/roles/:id', name: 'roles-edit', component: RoleForm},

        //Task manager
        // {path: '/kanban', name: 'kanban', component: IndexKanban},
        {path: '/kanban/:project_id', name: 'kanban', component: IndexKanban},
        {path: '/tasks', name: 'tasks', component: IndexTasks},

        //Projects
        {path: '/projects', name: 'projects', component: IndexProjects},
        {path: '/projects/table', name: 'projects-table', component: ProjectsTable},

        //Statistic for cost
        {path: '/cost', name: 'cost-table', component: IndexStatistic},

        //Cost Categories
        {path: '/cost/categories', name: 'cost-categories', component: IndexCostCategories},
    ]
});

window.axios.interceptors.response.use(undefined, (error) => {
    if (error.response && error.response.status === 401 || error.response && error.response.status === 419) {
        if (window.location.pathname !== '/auth') {
            window.location.href = '/auth';
        }
    }
    return Promise.reject(error);
});

window.axios.interceptors.response.use(undefined, (error) => {
    if (error.response && error.response.status === 403) {
        this.$router.push('/admin/access_denied');
    }
    return Promise.reject(error);
});

Vue.prototype.$axios = window.axios;
Vue.prototype.$configApi = window.configApi;

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
